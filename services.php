<?php
    $doctor_name = $_POST['doctor_name'];
    $email = $_POST['email'];
    $contact_no = $_POST['contact_no'];
    $address = $_POST['address'];
    $alt_address = $_POST['alt_address'];
    $patient_name = $_POST['patient_name'];
    $surgery_date = $_POST['surgery_date'];
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="content-language" content="en-US">
        <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="css/custom.css" type="text/css" />
        <style type="text/css">
            .card {
                margin-bottom:1em;
            }
            .card-header {
              font-weight: bold;
            }
        </style>
        
        
        
        
         
      
      
      
      
      
      
      
      
      <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        
        <link href="../images/new-favicon.png" rel="shortcut icon" type="image/png">

<!-- Stylesheet -->
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="../css/animate.css" rel="stylesheet" type="text/css">
<link href="../css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="../css/menuzord-skins/menuzord-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->

<!-- CSS | Preloader Styles -->

<!-- CSS | Custom Margin Padding Collection -->
<link href="../css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="../css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->



<!-- CSS | Theme Color -->
<link href="../css/colors/theme-skin-blue.css" rel="stylesheet" type="text/css">

<!-- external javascripts -->


<!-- JS | jquery plugin collection for this theme -->
<script src="../js/jquery-plugin-collection.js"></script>


        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    </head>
    <body>
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
      <div class="header-top bg-theme-colored sm-text-center" >
      <div class="container">
        <div class="row" style="margin-top:-2px;margin-bottom:-8px;">
          <div class="col-md-2">
            <div class="widget no-border m-10">
             <img src="http://image3dconversion.com/img/logo-white.png" width="100%">
            </div>
          </div>
          <div class="col-md-10">
            <div class="widget no-border m-0" style="font-size:13px;">
              <ul class="list-inline pull-right flip sm-pull-none sm-text-center mt-5">
                <li class="m-10 pl-10 pr-10 text-white" > Toll Free No: <img style="max-width:15px" src="../img/us.png"  alt="" > &nbsp;USA & Canada: <a href="tel:+18882769811" style="color:white;">(+1)888-276-9811</a> &nbsp;&nbsp; <img style="max-width:15px" src="../img/uk.png"  alt="" >&nbsp;UK: <a href="tel:+448000903841" style="color:white;">(+44)800-090-3841</a>
</a> </li>
               
                <li class="m-0 pl-10 pr-10 hidden-xs"> <i class="fa fa-envelope-o text-white"></i> <a class="text-white" href="mailto:info@image3dconversion.com">info@image3dconversion.com</a> </li>
               
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
    
    
    
        
        <br><center> <h2 class="text-theme-colored mt-0" style="color: #3d5b9b !important;margin-bottom: 10px;font-family: 'Droid Serif', sans-serif;
    font-weight: 700;
    line-height: 1.42857143;font-size: 30px;display: block;">Order Details
</h2>
<h3 class="text-gray mt-0 mt-sm-30 mb-0" style="margin-top: 30px !;    color: #808080;font-family: 'Droid Serif', sans-serif;
    font-weight: 700;">Please fill in the information required below for Implant Planning
</h3>

</center>


<hr><br>
        
        <div class="container">
            <div class="col-md-8" style="margin-left: auto;margin-right: auto;float: none;">
         
            <form method="post" action="finalprocess.php">
                <input name="doctor_name" type="hidden" value="<?php echo($doctor_name) ?>">
                <input name="email" type="hidden" value="<?php echo($email) ?>">
                <input name="contact_no" type="hidden" value="<?php echo($contact_no) ?>">
                <input name="address" type="hidden" value="<?php echo($address) ?>">
                <input name="alt_address" type="hidden" value="<?php echo($alt_address) ?>">
                <input name="patient_name" type="hidden" value="<?php echo($patient_name) ?>">
                <input name="surgery_date" type="hidden" value="<?php echo($surgery_date) ?>">
                
            <?php if($_POST['bone_segmentation'] == 1) include('services/bone_segmentation.php'); ?>
            
            <?php if($_POST['treatment_planning_design'] == 1) include('services/treatment_planning_design.php'); ?>
            
            <?php if($_POST['treatment_planning_design_fabrication'] == 1) include('services/treatment_planning_design_fabrication.php'); ?>
            
            <?php if($_POST['digital_denture'] == 1) include('services/digital_denture.php'); ?>
            
            <?php if($_POST['orthodentic_treatment'] == 1) include('services/orthodontic_treatment.php'); ?>
            
            <?php if($_POST['radiology_report'] == 1) include('services/radiology_report.php'); ?>
            
            <?php if($_POST['metal_components'] == 1) include('services/metal_components.php'); ?>
            
            <?php if($_POST['zygoma_guided_surgery'] == 1) include('services/zygoma_guided_surgery.php'); ?>
            
            <button type="button" class="btn btn-primary" onclick="goBack()">Back</button>
            <button type="submit" class="btn btn-primary">Preview Order</button>
            </form>
            </div>
            
            <br><br><br><BR>
        </div>
        <script>
            function goBack() {
              window.history.back();
            }
        </script>
    </body>
</html>