 <!-- Header -->
  <header id="header" class="header">
    <div class="header-top bg-theme-colored sm-text-center" >
      <div class="container">
        <div class="row" style="margin-top:-2px;margin-bottom:-8px;">
          <div class="col-md-2">
            <div class="widget no-border m-0">
              <ul class="styled-icons icon-dark icon-theme-colored icon-sm sm-text-center">
                <li><a href="https://www.facebook.com/image3dconversion/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://twitter.com/image3_d" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <li><a href="https://www.instagram.com/image3dconversion/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                <li><a href="https://www.linkedin.com/in/image3dconversion/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-10">
            <div class="widget no-border m-0" style="font-size:13px;">
              <ul class="list-inline pull-right flip sm-pull-none sm-text-center mt-5">
                <li class="m-0 pl-10 pr-10 text-white" > Toll Free No: <img style="max-width:15px" src="img/us.png"  alt="" > &nbsp;USA & Canada: <a href="tel:+18882769811" style="color:white;">(+1)888-276-9811</a> &nbsp;&nbsp; <img style="max-width:15px" src="img/us.png"  alt="" >&nbsp;UK: <a href="tel:+448000903841" style="color:white;">(+44)800-090-3841</a>
</a> </li>
               
                <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-white"></i> <a class="text-white" href="mailto:info@image3dconversion.com">info@image3dconversion.com</a> </li>
                <li class="sm-display-block mt-sm-10 mb-sm-10">
                  <!-- Modal: Appointment Starts -->
                  <a class="bg-light p-5 text-theme-colored font-14" href="doctor-login.php">&nbsp;&nbsp;UPLOAD A CASE&nbsp;&nbsp;</a>
                  <!-- Modal: Appointment End -->
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    <?php



$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
$navhome='1';
$navser='0';

if ((strpos($url,'about.php') !== false) || (strpos($url,'how-we-work.php') !== false) || (strpos($url,'testimonial.php') !== false)) {
   $navabout='1';
   $navhome='0';
} else {
   $navabout='0';
}






if ((strpos($url,'radiology-report.php') !== false) || (strpos($url,'bone-segmentation.php') !== false) || (strpos($url,'guided-surgery-planning.php') !== false) || (strpos($url,'surgical-guide.php') !== false) || (strpos($url,'orthodontic-treatment-planning.php') !== false) || (strpos($url,'provisional-prosthetic-planning.php') !== false) || (strpos($url,'digital-denture.php') !== false) || (strpos($url,'zygoma.php') !== false)) {
   $navservice='1';
   $navhome='0';
} else {
   $navservice='0';
}



if (strpos($url,'case-study.php') !== false) {
   $navcasestudy='1';
   $navhome='0';
} else {
   $navcasestudy='0';
}



if (strpos($url,'training.php') !== false) {
   $navtraining='1';
   $navhome='0';
} else {
   $navtraining='0';
}




if (strpos($url,'career.php') !== false) {
   $navcareer='1';
   $navhome='0';
} else {
   $navcareer='0';
}




if (strpos($url,'contact.php') !== false) {
   $navcontact='1';
   $navhome='0';
} else {
   $navcontact='0';
}




if (strpos($url,'doctor-login.php') !== false) {
     $navlogin='1';
   $navhome='0';
} else {
   $navlogin='0';
}




?>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    <div class="header-nav">
      <div class="header-nav-wrapper navbar-scrolltofixed bg-lightest" style="z-index: 1000;">
        <div class="container">
          <nav id="menuzord-right" class="menuzord blue bg-lightest menuzord-responsive"><a href="index.php" class="showhide" style="display: block;"></a>
            <a class="menuzord-brand pull-left flip" href="index.php">
              <img src="img/i3dlogo.png" alt="">
            </a>
            
            <ul class="menuzord-menu menuzord-right menuzord-indented scrollable" style="max-height: 400px; display: none;">
              <li <?php if($navhome==1) { echo 'class="active"'; } ?>><a href="index.php">Home<span class="indicator"></span></a> </li>
              <li <?php if($navabout==1) { echo 'class="active"'; } ?>><a href="about.php">About<span class="indicator"></span></a>
                <ul class="dropdown" style="right: auto; display: none;">
                  <li><a href="about.php">About Company</a></li>
                  <li><a href="how-we-work.php">How we work</a></li>
                  <li><a href="testimonial.php">Testimonials</a></li>
                </ul>
              </li>
              <li <?php if($navservice==1) { echo 'class="active"'; } ?>><a href="#">Services</a>  
               <ul class="dropdown" style="right: auto; display: none;">
                  <li><a href="radiology-report.php">Radiology Report</a></li>
                  <li><a href="bone-segmentation.php">Bone Segmentation</a></li>
                   <li><a href="guided-surgery-planning.php">Guided Surgery Planning</a></li>
                    <li><a href="surgical-guide.php">Surgical Guide</a></li>
                    <li><a href="orthodontic-treatment-planning.php">Orthodontic Treatment Planning</a></li>
                       <li><a href="provisional-prosthetic-planning.php">Provisional Prosthetic Planning</a></li>
                        <li><a href="digital-denture.php">Digital Denture</a></li>
                          <li><a href="zygoma.php">Zygoma & Pterygoid</a></li>
                 
                </ul>
              
              </li>           
              <li <?php if($navcasestudy==1) { echo 'class="active"'; } ?>><a href="case-study.php">Case Studies</a>
                         </li>
              <li><a href="i3d-liner">I3D Liner</a>
                 </li>
              <li <?php if($navtraining==1) { echo 'class="active"'; } ?>><a href="training.php">Training</a>
                
              </li>
                <li <?php if($navcareer==1) { echo 'class="active"'; } ?>><a href="career.php">Career</a>
                
              </li>
                <li <?php if($navcontact==1) { echo 'class="active"'; } ?>><a href="contact.php">Contact</a>
                
              </li>
              
                <li <?php if($navlogin==1) { echo 'class="active"'; } ?>><a href="#">Login<span class="indicator"></span></a>
                <ul class="dropdown" style="right: auto; display: none;">
                  <li><a href="doctor-login.php">Doctor Login</a></li>
                 
                 
                </ul>
              </li>
             
           </ul>
          </nav>
        </div>
      </div><div></div>
    </div>
  </header>