<div class="card">
    <div class="card-header bg-primary text-white">
        Metal Components
    </div>
    <div class="card-body">
        <div class="form-check">
            <input name="metal_components[guide_tubes]" class="form-check-input" type="checkbox" value="1" id="guide_tubes">
            <label class="form-check-label" for="guide_tubes">
                Guide Tubes ($10)
            </label>
        </div>
        <div class="form-check">
            <input name="metal_components[fixation_pins]" class="form-check-input" type="checkbox" value="1" id="fixation_pins">
            <label class="form-check-label" for="fixation_pins">
                Fixation Pins ($15)
            </label>
        </div>
    </div>
    <input name="services[metal_components]" value="1" type="hidden"/>
</div>