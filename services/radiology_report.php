<div class="card">
    <div class="card-header bg-primary text-white">
        Radiology Report - Flat Rate ($70)
    </div>
    <div class="card-body">
        <!--<div class="form-check">
            <input name="radiology_report[flat_rate]" class="form-check-input" type="checkbox" value="1" id="flat_rate">
            <label class="form-check-label" for="flat_rate">
                Flat Rate ($70)
            </label>
        </div>
        <hr/>-->
        <input name="radiology_report[flat_rate]" type="hidden" value="1">
        <div class="form-group">
            <label for="study_of_purpose">Study of Purpose</label>
            <input id="study_of_purpose" name="radiology_report[study_of_purpose]" type="text" class="form-control" aria-describedby="" placeholder="">
        </div>
        <div class="form-group">
            <label for="job_description">Job Description of CBCT Scan</label>
            <input id="job_description" name="radiology_report[job_description]" type="text" class="form-control" aria-describedby="" placeholder="">
        </div>
        <div class="form-group">
            <label for="date_of_birth">Date of Birth</label>
            <input id="date_of_birth" name="radiology_report[date_of_birth]" type="date" class="form-control" aria-describedby="" placeholder="">
        </div>
    </div>
    <input name="services[radiology_report]" value="1" type="hidden"/>
</div>