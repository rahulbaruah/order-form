<div class="card">
    <div class="card-header bg-primary text-white">
        Treatment Planning (Design & Fabrication)
    </div>
    <div class="card-body">
        <div class="form-check">
            <input name="treatment_planning_design_fabrication[bone_reduction_case]" class="form-check-input" type="checkbox" value="1" id="bone_reduction_case3">
            <label class="form-check-label" for="bone_reduction_case3">
                Bone Reduction Case ($599)
            </label>
        </div>
        <div class="form-check">
            <input name="treatment_planning_design_fabrication[tooth_supported_case]" class="form-check-input" type="checkbox" value="1" id="tooth_supported_case3">
            <label class="form-check-label" for="tooth_supported_case3">
                Tooth Supported Case ($99 on first implant + $20 Additional Implant)
            </label>
        </div>
        <div class="form-check">
            <input name="treatment_planning_design_fabrication[tissue_supported_case]" class="form-check-input" type="checkbox" value="1" id="tissue_supported_case3">
            <label class="form-check-label" for="tissue_supported_case3">
                Tissue Supported Case ($299)
            </label>
        </div>
        <div class="form-check">
            <input name="treatment_planning_design_fabrication[bone_supported_case]" class="form-check-input" type="checkbox" value="1" id="bone_supported_case3">
            <label class="form-check-label" for="bone_supported_case3">
                Bone Supported Case ($399)
            </label>
        </div>
        <div class="form-check">
            <input name="treatment_planning_design_fabrication[provisional_planning]" class="form-check-input" type="checkbox" value="1" id="provisional_planning3">
            <label class="form-check-label" for="provisional_planning3">
                Provisional Planning ($150)
            </label>
        </div>
        <hr/>
        <div class="form-group">
            <label for="no_of_implants3">Number of Implants</label>
            <input id="no_of_implants3" name="treatment_planning_design_fabrication[no_of_implants]" type="number" class="form-control" aria-describedby="" placeholder="">
        </div>
        <div class="form-group">
            <label for="preferred_implant3">Preferred Implant</label>
            <input id="preferred_implant3" name="treatment_planning_design_fabrication[preferred_implant]" type="text" class="form-control" aria-describedby="" placeholder="">
        </div> 
        <div class="form-group">
            <label for="jaw_information3">Jaw Information</label>
            <select name="treatment_planning_design_fabrication[jaw_information]" class="form-control" id="jaw_information3">
                <option value="" disabled selected>Select</option>
                <option value="mandible">Mandible</option>
                <option value="maxilla">Maxilla</option>
                <option value="both">Both</option>
            </select>
        </div>
        <div class="form-group">
            <label for="restorative_plan3">Restorative Plan</label>
            <input id="restorative_plan3" name="treatment_planning_design_fabrication[restorative_plan]" type="text" class="form-control" aria-describedby="" placeholder="">
        </div>
        <!--<div class="form-group">
            <label for="surgery_date3">Surgery Date</label>
            <input id="surgery_date3" name="treatment_planning_design_fabrication[surgery_date]" type="text" class="form-control" aria-describedby="" placeholder="">
        </div> -->
        <div class="form-group">
            <label for="provisional_planning3">Provisional Planning</label>
            <select name="treatment_planning_design_fabrication[provisional_planning_reqd]" class="form-control" id="provisional_planning3">
              <option value="" disabled selected>Select</option>
              <option value="yes">Yes</option>
              <option value="no">No</option>
            </select>
        </div>
    </div>
    <input name="services[treatment_planning_design_fabrication]" value="1" type="hidden"/>
</div>