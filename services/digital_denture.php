<div class="card">
    <div class="card-header bg-primary text-white">
        Digital Dentures
    </div>
    <div class="card-body">
        <div class="form-check">
            <input name="digital_denture[print]" class="form-check-input" type="checkbox" value="1" id="print">
            <label class="form-check-label" for="print">
                Print ($65)
            </label>
        </div>
        <div class="form-check">
            <input name="digital_denture[printing_planning]" class="form-check-input" type="checkbox" value="1" id="printing_planning">
            <label class="form-check-label" for="printing_planning">
                Printing & Planning ($135)
            </label>
        </div>
        <hr/>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="digital_denture[teeth_purchase_print]" id="purchase_teeth" value="purchase_teeth">
          <label class="form-check-label" for="purchase_teeth">
            I will purchase the teeth
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="digital_denture[teeth_purchase_print]" id="print_teeth" value="print_teeth">
          <label class="form-check-label" for="print_teeth">
            I will print the teeth
          </label>
        </div>
    </div>
    <input name="services[digital_denture]" value="1" type="hidden"/>
</div>