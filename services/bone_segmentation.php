<div class="card">
    <div class="card-header bg-primary text-white">
        Bone Segmentation
    </div>
    <div class="card-body">
        <div class="form-check">
            <input name="bone_segmentation[basic_conversion]" class="form-check-input" type="checkbox" value="1" id="basic_conversion">
            <label class="form-check-label" for="basic_conversion">
                Basic Conversion ($50)
            </label>
        </div>
        <div class="form-check">
            <input name="bone_segmentation[virtual_extraction]" class="form-check-input" type="checkbox" value="1" id="virtual_extraction">
            <label class="form-check-label" for="virtual_extraction">
                Virtual Extraction ($75)
            </label>
        </div>
        <div class="form-check">
            <input name="bone_segmentation[zygomatic_conversion]" class="form-check-input" type="checkbox" value="1" id="zygomatic_conversion">
            <label class="form-check-label" for="zygomatic_conversion">
                Zygomatic Conversion ($99)
            </label>
        </div>
        <hr/>
        <div class="form-group">
            <label for="jaw_information">Jaw Information</label>
            <select name="bone_segmentation[jaw_information]" class="form-control" id="jaw_information">
                <option value="" disabled selected>Select</option>
                <option value="mandible">Mandible</option>
                <option value="maxilla">Maxilla</option>
                <option value="both">Both</option>
            </select>
        </div>
        <div class="form-group">
            <label for="order_description">Order Description (If Any)</label>
            <textarea id="order_description" name="bone_segmentation[order_description]" class="form-control" aria-describedby="" placeholder=""></textarea>
        </div>
    </div>
    <input name="services[bone_segmentation]" value="1" type="hidden"/>
</div>