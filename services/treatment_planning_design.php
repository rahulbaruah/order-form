<div class="card">
    <div class="card-header bg-primary text-white">
        Treatment Planning (Design Only)
    </div>
    <div class="card-body">
        <div class="form-check">
            <input name="treatment_planning_design[bone_reduction_case]" class="form-check-input" type="checkbox" value="1" id="bone_reduction_case">
            <label class="form-check-label" for="bone_reduction_case">
                Bone Reduction Case ($449)
            </label>
        </div>
        <div class="form-check">
            <input name="treatment_planning_design[tooth_supported_case]" class="form-check-input" type="checkbox" value="1" id="tooth_supported_case">
            <label class="form-check-label" for="tooth_supported_case">
                Tooth Supported Case ($55 on first implant + $20 Additional Implant)
            </label>
        </div>
        <div class="form-check">
            <input name="treatment_planning_design[tissue_supported_case]" class="form-check-input" type="checkbox" value="1" id="tissue_supported_case">
            <label class="form-check-label" for="tissue_supported_case">
                Tissue Supported Case ($199)
            </label>
        </div>
        <div class="form-check">
            <input name="treatment_planning_design[bone_supported_case]" class="form-check-input" type="checkbox" value="1" id="bone_supported_case">
            <label class="form-check-label" for="bone_supported_case">
                Bone Supported Case ($299)
            </label>
        </div>
        <div class="form-check">
            <input name="treatment_planning_design[provisional_planning]" class="form-check-input" type="checkbox" value="1" id="provisional_planning">
            <label class="form-check-label" for="provisional_planning">
                Provisional Planning ($75)
            </label>
        </div>
        <hr/>
        <div class="form-group">
            <label for="no_of_implants2">Number of Implants</label>
            <input id="no_of_implants2" name="treatment_planning_design[no_of_implants]" type="number" class="form-control" aria-describedby="" placeholder="">
        </div>
        <div class="form-group">
            <label for="preferred_implant2">Preferred Implant</label>
            <input id="preferred_implant2" name="treatment_planning_design[preferred_implant]" type="text" class="form-control" aria-describedby="" placeholder="">
        </div> 
        <div class="form-group">
            <label for="jaw_information2">Jaw Information</label>
            <select name="treatment_planning_design[jaw_information]" class="form-control" id="jaw_information2">
                <option value="" disabled selected>Select</option>
                <option value="mandible">Mandible</option>
                <option value="maxilla">Maxilla</option>
                <option value="both">Both</option>
            </select>
        </div>
        <div class="form-group">
            <label for="restorative_plan2">Restorative Plan</label>
            <input id="restorative_plan2" name="treatment_planning_design[restorative_plan]" type="text" class="form-control" aria-describedby="" placeholder="">
        </div>
        <!--<div class="form-group">
            <label for="surgery_date2">Surgery Date</label>
            <input id="surgery_date2" name="treatment_planning_design[surgery_date]" type="text" class="form-control" aria-describedby="" placeholder="">
        </div> -->
        <div class="form-group">
            <label for="provisional_planning2">Provisional Planning</label>
            <select name="treatment_planning_design[provisional_planning_reqd]" class="form-control" id="provisional_planning2">
              <option value="" disabled selected>Select</option>
              <option value="yes">Yes</option>
              <option value="no">No</option>
            </select>
        </div>
    </div>
    <input name="services[treatment_planning_design]" value="1" type="hidden"/>
</div>