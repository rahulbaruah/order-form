<div class="card">
    <div class="card-header bg-primary text-white">
        Orthodentic Treatment
    </div>
    <div class="card-body">
        <div class="form-check">
            <input name="orthodentic_treatment[ortho_planning]" class="form-check-input" type="checkbox" value="1" id="ortho_planning">
            <label class="form-check-label" for="ortho_planning">
                Ortho Planning ($99)
            </label>
        </div>
        <div class="form-check">
            <input name="orthodentic_treatment[ortho_model_print]" class="form-check-input" type="checkbox" value="1" id="ortho_model_print">
            <label class="form-check-label" for="ortho_model_print">
                Ortho Model Print ($15 each)
            </label>
        </div>
        <hr/>
        <div class="form-group">
            <label for="no_of_prints">Number of Prints (For Ortho Model Print)</label>
            <input id="no_of_prints" name="orthodentic_treatment[no_of_prints]" type="number" class="form-control" aria-describedby="" placeholder="">
        </div>
        <div class="form-group">
            <label for="patient_photos">Patient Photos</label>
            <p id="patient_photos" class="help-block">Upload after completing this form</p>
        </div>
        <div class="form-group">
            <label for="order_description5">Order Description</label>
            <textarea id="order_description5" name="orthodentic_treatment[order_description]" class="form-control" aria-describedby="" placeholder=""></textarea>
        </div>
        <div class="form-group">
            <label for="jaw_information5">Jaw Information</label>
            <select name="orthodentic_treatment[jaw_information]" class="form-control" id="jaw_information5">
                <option value="" disabled selected>Select</option>
                <option value="mandible">Mandible</option>
                <option value="maxilla">Maxilla</option>
                <option value="both">Both</option>
            </select>
        </div>
        <div class="form-group">
            <label for="aligner_frequency">Aligner Frequency</label>
            <input id="aligner_frequency" name="orthodentic_treatment[aligner_frequency]" type="text" class="form-control" aria-describedby="" placeholder="">
        </div>
        <div class="form-group">
            <label for="anterior_movement">Anterior Movement or Fullarch adjustment</label>
            <input id="anterior_movement" name="orthodentic_treatment[anterior_movement]" type="text" class="form-control" aria-describedby="" placeholder="">
        </div>
        <div class="form-group">
            <label for="chief_complaint">Chief Complaint</label>
            <input id="chief_complaint" name="orthodentic_treatment[chief_complaint]" type="text" class="form-control" aria-describedby="" placeholder="">
        </div>
    </div>
    <input name="services[orthodentic_treatment]" value="1" type="hidden"/>
</div>