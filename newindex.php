<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="content-language" content="en-US">
        <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="css/custom.css" type="text/css" />
        <style type="text/css">
            #alt_address_check_box input, #alt_address_check_box label {
                cursor:pointer;
            }
            .card {
                margin-bottom:1em;
            }
            .card-header {
              font-weight: bold;
            }
        </style>
    </head>
    <body>
        
         <?php include 'header.php'; ?>
         
        <div class="container">
          <div class="col-md-8" style="margin-left: auto;margin-right: auto;float: none;">
        <form method="post" action="services.php">
            <!-------------Doctor Information---------------->
            <div class="card">
              <div class="card-header bg-primary text-white">
                Doctor Information
              </div>
              <div class="card-body">
                <div class="form-group">
                    <label for="doctor_name">Doctor's Name</label>
                    <input id="doctor_name" name="doctor_name" type="text" class="form-control" aria-describedby="" placeholder="">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" name="email" type="email" class="form-control" aria-describedby="" placeholder="">
                </div>
                <div class="form-group">
                    <label for="contact_no">Contact Number</label>
                    <input id="contact_no" name="contact_no" type="text" class="form-control" aria-describedby="" placeholder="">
                </div>
                <div class="form-group">
                    <label for="address">Address</label>
                    <textarea id="address" name="address" class="form-control" aria-describedby="" placeholder="" rows="3"></textarea>
                </div>
                <div class="form-check" id="alt_address_check_box">
                  <input name="altAddrCheck" class="form-check-input" type="checkbox" value="1" id="altAddrCheck">
                  <label class="form-check-label" for="altAddrCheck">
                    Ship to a Different Address
                  </label>
                </div>
                <div class="form-group" id="alt_address_box">
                    <textarea name="alt_address" class="form-control" aria-describedby="" placeholder="" rows="3"></textarea>
                </div>
              </div>
            </div>
            <!-------------Patient Information---------------->
            <div class="card">
              <div class="card-header bg-primary text-white">
                Patient Information
              </div>
              <div class="card-body">
                <div class="form-group">
                    <label for="patient_name">Patient Name</label>
                    <input id="patient_name" name="patient_name" type="text" class="form-control" aria-describedby="" placeholder="">
                </div>
                <div class="form-group">
                    <label for="surgery_date">Surgery Date</label>
                    <input id="surgery_date" name="surgery_date" type="date" class="form-control" aria-describedby="" placeholder="">
                </div>
              </div>
            </div>
            <!-------------Services List---------------->
            <div class="card">
              <div class="card-header bg-primary text-white">
                Select Services
              </div>
              <div class="card-body">
                <div class="form-check">
                  <input name="bone_segmentation" class="form-check-input" type="checkbox" value="1" id="bone_segmentation">
                  <label class="form-check-label" for="bone_segmentation">
                    Bone Segmentation
                  </label>
                </div>
                <div class="form-check">
                  <input name="treatment_planning_design" class="form-check-input" type="checkbox" value="1" id="treatment_planning_design">
                  <label class="form-check-label" for="treatment_planning_design">
                    Treatment Planning (Design Only)
                  </label>
                </div>
                <div class="form-check">
                  <input name="treatment_planning_design_fabrication" class="form-check-input" type="checkbox" value="1" id="treatment_planning_design_fabrication">
                  <label class="form-check-label" for="treatment_planning_design_fabrication">
                    Treatment Planning (Design & Fabrication)
                  </label>
                </div>
                <div class="form-check">
                  <input name="digital_denture" class="form-check-input" type="checkbox" value="1" id="digital_denture">
                  <label class="form-check-label" for="digital_denture">
                    Digital Denture
                  </label>
                </div>
                <div class="form-check">
                  <input name="orthodentic_treatment" class="form-check-input" type="checkbox" value="1" id="orthodentic_treatment">
                  <label class="form-check-label" for="orthodentic_treatment">
                    Orthodontic Treatment
                  </label>
                </div>
                <div class="form-check">
                  <input name="radiology_report" class="form-check-input" type="checkbox" value="1" id="radiology_report">
                  <label class="form-check-label" for="radiology_report">
                    Radiology Report
                  </label>
                </div>
                <div class="form-check">
                  <input name="metal_components" class="form-check-input" type="checkbox" value="1" id="metal_components">
                  <label class="form-check-label" for="metal_components">
                    Metal Components
                  </label>
                </div>
                <div class="form-check">
                  <input name="zygoma_guided_surgery" class="form-check-input" type="checkbox" value="1" id="zygoma_guided_surgery">
                  <label class="form-check-label" for="zygoma_guided_surgery">
                    Zygoma Guided Surgery (Design Only)
                  </label>
                </div>
              </div>
            </div>
            
            <button type="submit" class="btn btn-primary">Continue to Order Description</button>
            
        </form>
          </div>
        </div>
<!--<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>-->
<!--<script src="https://kit.fontawesome.com/18c7cf27e5.js" crossorigin="anonymous"></script>-->
<script type="text/javascript">
(function() {
    const alt_address_box = document.getElementById('alt_address_box');
    
    alt_address_box.style.display='none';
	
	document.getElementById ("altAddrCheck").addEventListener ("change", (event) => {
      if (event.target.checked) {
        alt_address_box.style.display='block';
      } else {
        alt_address_box.style.display='none';
      }
    });
})();
</script>        
        
    </body>
</html>