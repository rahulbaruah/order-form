<?php
/*function dd($data){
  highlight_string("<?php\n " . var_export($data, true) . "?>");
  echo '<script>document.getElementsByTagName("code")[0].getElementsByTagName("span")[1].remove() ;document.getElementsByTagName("code")[0].getElementsByTagName("span")[document.getElementsByTagName("code")[0].getElementsByTagName("span").length - 1].remove() ; </script>';
  die();
}*/

function styleString($string) {
    return ucwords(str_replace("_"," ",$string));
}

/*dd($_POST);*/

/*--------------COST-------------------*/
$bone_segmentation_cost['basic_conversion'] = 50;
$bone_segmentation_cost['virtual_extraction'] = 75;
$bone_segmentation_cost['zygomatic_conversion'] = 99;

$treatment_planning_design_cost['bone_reduction_case'] = 449;
$treatment_planning_design_cost['tooth_supported_case'] = 55;
$treatment_planning_design_cost['tooth_supported_case_additional'] = 20;
$treatment_planning_design_cost['tissue_supported_case'] = 199;
$treatment_planning_design_cost['bone_supported_case'] = 299;
$treatment_planning_design_cost['provisional_planning'] = 75;

$treatment_planning_design_fabrication_cost['bone_reduction_case'] = 599;
$treatment_planning_design_fabrication_cost['tooth_supported_case'] = 99;
$treatment_planning_design_fabrication_cost['tooth_supported_case_additional'] = 20;
$treatment_planning_design_fabrication_cost['tissue_supported_case'] = 299;
$treatment_planning_design_fabrication_cost['bone_supported_case'] = 399;
$treatment_planning_design_fabrication_cost['provisional_planning'] = 150;

$digital_denture_cost['print'] = 65;
$digital_denture_cost['printing_planning'] = 135;

$orthodentic_treatment_cost['ortho_planning'] = 99;
$orthodentic_treatment_cost['ortho_model_print'] = 15;

$radiology_report_cost['flat_rate'] = 70;

$metal_components_cost['guide_tubes'] = 10;
$metal_components_cost['fixation_pins'] = 15;

$zygoma_guided_surgery_cost['quadrant_pterygoid'] = 323;
$zygoma_guided_surgery_cost['full_pterygoid'] = 473;
/*--------------//COST-------------------*/

$services = $_POST['services'];

$total_cost = 0;
$message = '<html><body>';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="content-language" content="en-US">
        <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="css/custom.css" type="text/css" />
        <style type="text/css">
            .card {
                margin-bottom:1em;
            }
        </style>
        
        
        
        
        
        
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        
        <link href="../images/new-favicon.png" rel="shortcut icon" type="image/png">

<!-- Stylesheet -->
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="../css/animate.css" rel="stylesheet" type="text/css">
<link href="../css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="../css/menuzord-skins/menuzord-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->

<!-- CSS | Preloader Styles -->

<!-- CSS | Custom Margin Padding Collection -->
<link href="../css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="../css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->



<!-- CSS | Theme Color -->
<link href="../css/colors/theme-skin-blue.css" rel="stylesheet" type="text/css">

<!-- external javascripts -->


<!-- JS | jquery plugin collection for this theme -->
<script src="../js/jquery-plugin-collection.js"></script>


        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    </head>
    <body>
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
      <div class="header-top bg-theme-colored sm-text-center" >
      <div class="container">
        <div class="row" style="margin-top:-2px;margin-bottom:-8px;">
          <div class="col-md-2">
            <div class="widget no-border m-10">
             <img src="http://image3dconversion.com/img/logo-white.png" width="100%">
            </div>
          </div>
          <div class="col-md-10">
            <div class="widget no-border m-0" style="font-size:13px;">
              <ul class="list-inline pull-right flip sm-pull-none sm-text-center mt-5">
                <li class="m-10 pl-10 pr-10 text-white" > Toll Free No: <img style="max-width:15px" src="../img/us.png"  alt="" > &nbsp;USA & Canada: <a href="tel:+18882769811" style="color:white;">(+1)888-276-9811</a> &nbsp;&nbsp; <img style="max-width:15px" src="../img/uk.png"  alt="" >&nbsp;UK: <a href="tel:+448000903841" style="color:white;">(+44)800-090-3841</a>
</a> </li>
               
                <li class="m-0 pl-10 pr-10 hidden-xs"> <i class="fa fa-envelope-o text-white"></i> <a class="text-white" href="mailto:info@image3dconversion.com">info@image3dconversion.com</a> </li>
               
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        <br><center> <h2 class="text-theme-colored mt-0" style="color: #3d5b9b !important;margin-bottom: 10px;font-family: 'Droid Serif', sans-serif;
    font-weight: 700;
    line-height: 1.42857143;font-size: 30px;display: block;">Order Preview
</h2>


</center>


<hr><br>
        <div class="container">
            <div class="col-md-8" style="margin-left: auto;margin-right: auto;float: none;">
            <form method="POST" action="finalsubmit.php">
<?php
    $doctor_name = $_POST['doctor_name'];
    $email = $_POST['email'];
    $contact_no = $_POST['contact_no'];
    $address = $_POST['address'];
    $alt_address = $_POST['alt_address'];
    $patient_name = $_POST['patient_name'];
    $surgery_date = $_POST['surgery_date'];
?>
                <input name="doctor_name" type="hidden" value="<?php echo($doctor_name) ?>">
                <input name="email" type="hidden" value="<?php echo($email) ?>">
                <input name="contact_no" type="hidden" value="<?php echo($contact_no) ?>">
                <input name="address" type="hidden" value="<?php echo($address) ?>">
                <input name="alt_address" type="hidden" value="<?php echo($alt_address) ?>">
                <input name="patient_name" type="hidden" value="<?php echo($patient_name) ?>">
                <input name="surgery_date" type="hidden" value="<?php echo($surgery_date) ?>">
                <input name="services" type="hidden" value='<?php echo(json_encode($services)) ?>'>
    
    <div class="card">
        <div class="card-header bg-primary text-white">
                Doctor Information
        </div>
        <?php $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">'; ?>
        <div class="card-body">
            <dl class="row">
              <dt class="col-sm-3">Doctor's Name</dt>
              <dd class="col-sm-9"><?php echo($doctor_name) ?></dd>
              <?php $message .= "<tr><td><strong>Doctor's Name:</strong> </td><td>" . $doctor_name . "</td></tr>"; ?>
              
              <dt class="col-sm-3">Email</dt>
              <dd class="col-sm-9"><?php echo($email) ?></dd>
              <?php $message .= "<tr><td><strong>Email:</strong> </td><td>" . $email . "</td></tr>"; ?>
              
              <dt class="col-sm-3">Contact Number</dt>
              <dd class="col-sm-9"><?php echo($contact_no) ?></dd>
              <?php $message .= "<tr><td><strong>Contact Number:</strong> </td><td>" . $contact_no . "</td></tr>"; ?>
              
              <dt class="col-sm-3">Address</dt>
              <dd class="col-sm-9"><?php echo($address) ?></dd>
              <?php $message .= "<tr><td><strong>Address:</strong> </td><td>" . $address . "</td></tr>"; ?>
              
              <dt class="col-sm-3">Shipping Address</dt>
              <dd class="col-sm-9"><?php echo($alt_address) ?></dd>
              <?php $message .= "<tr><td><strong>Shipping Address:</strong> </td><td>" . $alt_address . "</td></tr>"; ?>
            </dl>
        </div>
    </div>
    
    <div class="card">
        <div class="card-header bg-primary text-white">
                Patient Information
        </div>
        <div class="card-body">
            <dl class="row">
              <dt class="col-sm-3">Patient Name</dt>
              <dd class="col-sm-9"><?php echo($patient_name) ?></dd>
              <?php $message .= "<tr><td><strong>Patient Name:</strong> </td><td>" . $patient_name . "</td></tr>"; ?>
              
              <dt class="col-sm-3">Surgery Date</dt>
              <dd class="col-sm-9"><?php echo($surgery_date) ?></dd>
              <?php $message .= "<tr><td><strong>Surgery Date:</strong> </td><td>" . $surgery_date . "</td></tr>"; ?>
            </dl>
        </div>
    </div>
    <?php $message .= "</table>"; ?>
   
   <div class="card">
        <div class="card-header bg-primary text-white">
                Services with Cost
        </div>
        <?php $message .= "<hr/>"; ?>
        <?php $message .= "<h2>Services with Cost</h2>"; ?>
        <div class="card-body">
<?php
if($services['bone_segmentation'] == 1) {
    echo('<strong><u>Bone Segmentation</u></strong>');
    echo('<br/>');
    $message .= "<strong><u>Bone Segmentation</u></strong>";
    $message .= "<br/>";
    
    $bone_segmentation = $_POST['bone_segmentation'];
    foreach($bone_segmentation as $key=>$value) {
        if($value == 1) {
            if(isset($bone_segmentation_cost[$key])) {
                $total_cost += $bone_segmentation_cost[$key];
                echo(styleString($key).': $'.$bone_segmentation_cost[$key]);
                echo('<br/>');
                
                $message .= styleString($key).': $'.$bone_segmentation_cost[$key];
                $message .= "<br/>";
            }
        } ?>
        <input name="bone_segmentation[<?php echo($key) ?>]" type="hidden" value="<?php echo($value) ?>"/>
<?php    }
    $message .= "Jaw Information: ".$bone_segmentation["jaw_information"];
    echo("Jaw Information: ".$bone_segmentation["jaw_information"]);
    echo('<br/>');
    $message .= "Order Description: ".$bone_segmentation["order_description"];
    echo("Order Description: ".$bone_segmentation["order_description"]);
    echo('<br/>');
}

if($services['treatment_planning_design'] == 1) {
    echo('<strong><u>Treatment Planning (Design Only)</u></strong>');
    echo('<br/>');
    $message .= "<strong><u>Treatment Planning (Design Only)</u></strong>";
    $message .= "<br/>";
    
    $treatment_planning_design = $_POST['treatment_planning_design'];
    foreach($treatment_planning_design as $key=>$value) {
        if($value == 1) {
            if(isset($treatment_planning_design_cost[$key])) {
                $total_cost += $treatment_planning_design_cost[$key];
                echo(styleString($key).': $'.$treatment_planning_design_cost[$key]);
                echo('<br/>');
                $message .= styleString($key).': $'.$treatment_planning_design_cost[$key];
                $message .= "<br/>";
            }
        } ?>
        <input name="treatment_planning_design[<?php echo($key) ?>]" type="hidden" value="<?php echo($value) ?>"/>
<?php    }
    if($treatment_planning_design['tooth_supported_case'] && ($treatment_planning_design['no_of_implants'] > 1)) {
        $temp_calc = $treatment_planning_design_cost['tooth_supported_case_additional'] * (abs($treatment_planning_design['no_of_implants']) - 1);
        $total_cost += $temp_calc;
        echo('Additional Implant Cost: $'.$treatment_planning_design_cost['tooth_supported_case_additional'].' x '.(abs($treatment_planning_design['no_of_implants']) - 1).' = $'.$temp_calc);
        echo('<br/>');
        $message .= 'Additional Implant Cost: $'.$treatment_planning_design_cost['tooth_supported_case_additional'].' x '.(abs($treatment_planning_design['no_of_implants']) - 1).' = $'.$temp_calc;
        $message .= "<br/>";
    }
}

if($services['treatment_planning_design_fabrication'] == 1) {
    echo('<strong><u>Treatment Planning (Design & Fabrication)</u></strong>');
    echo('<br/>');
    $message .= "<strong><u>Treatment Planning (Design & Fabrication)</u></strong>";
    $message .= "<br/>";
    
    $treatment_planning_design_fabrication = $_POST['treatment_planning_design_fabrication'];
    foreach($treatment_planning_design_fabrication as $key=>$value) {
        if($value == 1) {
            if(isset($treatment_planning_design_fabrication_cost[$key])) {
                $total_cost += $treatment_planning_design_fabrication_cost[$key];
                echo(styleString($key).': $'.$treatment_planning_design_fabrication_cost[$key]);
                echo('<br/>');
                $message .= styleString($key).': $'.$treatment_planning_design_fabrication_cost[$key];
                $message .= "<br/>";
            }
        } ?>
        <input name="treatment_planning_design_fabrication[<?php echo($key) ?>]" type="hidden" value="<?php echo($value) ?>"/>
<?php    }
    if($treatment_planning_design_fabrication['tooth_supported_case'] && ($treatment_planning_design_fabrication['no_of_implants'] > 1)) {
        $temp_calc = $treatment_planning_design_fabrication_cost['tooth_supported_case_additional'] * (abs($treatment_planning_design_fabrication['no_of_implants']) - 1);
        $total_cost += $temp_calc;
        echo('Additional Implant Cost: $'.$treatment_planning_design_fabrication_cost['tooth_supported_case_additional'].' x '.(abs($treatment_planning_design_fabrication['no_of_implants']) - 1).' = $'.$temp_calc);
        echo('<br/>');
        $message .= 'Additional Implant Cost: $'.$treatment_planning_design_fabrication_cost['tooth_supported_case_additional'].' x '.(abs($treatment_planning_design_fabrication['no_of_implants']) - 1).' = $'.$temp_calc;
        $message .= "<br/>";
    }
}

if($services['digital_denture'] == 1) {
    echo('<strong><u>Digital Dentures</u></strong>');
    echo('<br/>');
    $message .= "<strong><u>Digital Dentures</u></strong>";
    $message .= "<br/>";
    
    $digital_denture = $_POST['digital_denture'];
    foreach($digital_denture as $key=>$value) {
        if($value == 1) {
            if(isset($digital_denture_cost[$key])) {
                $total_cost += $digital_denture_cost[$key];
                echo(styleString($key).': $'.$digital_denture_cost[$key]);
                echo('<br/>');
                $message .= styleString($key).': $'.$digital_denture_cost[$key];
                $message .= "<br/>";
            }
        } ?>
        <input name="digital_denture[<?php echo($key) ?>]" type="hidden" value="<?php echo($value) ?>"/>
<?php    }
}

if($services['orthodentic_treatment'] == 1) {
    echo('<strong><u>Orthodentic Treatment</u></strong>');
    echo('<br/>');
    $message .= "<strong><u>Orthodentic Treatment</u></strong>";
    $message .= "<br/>";
    
    $orthodentic_treatment = $_POST['orthodentic_treatment'];
    foreach($orthodentic_treatment as $key=>$value) {
        if($value == 1) {
            if(isset($orthodentic_treatment_cost[$key])) {
                if($key == 'ortho_model_print') {
                    $temp_calc = $orthodentic_treatment_cost[$key] * $orthodentic_treatment['no_of_prints'];
                    $total_cost += $temp_calc;
                    echo('Ortho Model Print: $'.$orthodentic_treatment_cost[$key].' x '.abs($orthodentic_treatment['no_of_prints']).' = $'.$temp_calc);
                    echo('<br/>');
                    $message .= 'Ortho Model Print: $'.$orthodentic_treatment_cost[$key].' x '.abs($orthodentic_treatment['no_of_prints']).' = $'.$temp_calc;
                    $message .= "<br/>";
                } else {
                    $total_cost += $orthodentic_treatment_cost[$key];
                    echo(styleString($key).': $'.$orthodentic_treatment_cost[$key]);
                    echo('<br/>');
                    $message .= styleString($key).': $'.$orthodentic_treatment_cost[$key];
                    $message .= "<br/>";
                }
            }
        } ?>
        <input name="orthodentic_treatment[<?php echo($key) ?>]" type="hidden" value="<?php echo($value) ?>"/>
<?php    }
}

if($services['radiology_report'] == 1) {
    echo('<strong><u>Radiology Report</u></strong>');
    echo('<br/>');
    $message .= "<strong><u>Radiology Report</u></strong>";
    $message .= "<br/>";
    
    $radiology_report = $_POST['radiology_report'];
    foreach($radiology_report as $key=>$value) {
        if($value == 1) {
            if(isset($radiology_report_cost[$key])) {
                $total_cost += $radiology_report_cost[$key];
                echo(styleString($key).': $'.$radiology_report_cost[$key]);
                echo('<br/>');
                $message .= styleString($key).': $'.$radiology_report_cost[$key];
                $message .= "<br/>";
            }
        } ?>
        <input name="radiology_report[<?php echo($key) ?>]" type="hidden" value="<?php echo($value) ?>"/>
<?php    }
}

if($services['metal_components'] == 1) {
    echo('<strong><u>Metal Components</u></strong>');
    echo('<br/>');
    $message .= "<strong><u>Metal Components</u></strong>";
    $message .= "<br/>";
    
    $metal_components = $_POST['metal_components'];
    foreach($metal_components as $key=>$value) {
        if($value == 1) {
            if(isset($metal_components_cost[$key])) {
                $total_cost += $metal_components_cost[$key];
                echo(styleString($key).': $'.$metal_components_cost[$key]);
                echo('<br/>');
                $message .= styleString($key).': $'.$metal_components_cost[$key];
                $message .= "<br/>";
            }
        } ?>
        <input name="metal_components[<?php echo($key) ?>]" type="hidden" value="<?php echo($value) ?>"/>
<?php    }
}

if($services['zygoma_guided_surgery'] == 1) {
    echo('<strong><u>Zygoma Guided Surgery (Design Only)</u></strong>');
    echo('<br/>');
    $message .= "<strong><u>Zygoma Guided Surgery (Design Only)</u></strong>";
    $message .= "<br/>";
    
    $zygoma_guided_surgery = $_POST['zygoma_guided_surgery'];
    foreach($zygoma_guided_surgery as $key=>$value) {
        if($value == 1) {
            if(isset($zygoma_guided_surgery_cost[$key])) {
                $total_cost += $zygoma_guided_surgery_cost[$key];
                echo(styleString($key).': $'.$zygoma_guided_surgery_cost[$key]);
                echo('<br/>');
                $message .= styleString($key).': $'.$zygoma_guided_surgery_cost[$key];
                $message .= "<br/>";
            }
        } ?>
        <input name="zygoma_guided_surgery[<?php echo($key) ?>]" type="hidden" value="<?php echo($value) ?>"/>
<?php    }
}

echo("<hr/>");
echo("<h4>Total: $".$total_cost."</h4>");
$message .= "<hr/>";
$message .= "<h4>Total: $".$total_cost."</h4>";
    
?>
<input name="total" type="hidden" value="<?php echo($total_cost) ?>"/>
<input name="message" type="hidden" value="<?php echo(htmlentities($message)) ?>"/>
    </div>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    </div>
    
       <br><br><br><BR>
</div>
    </body>
</html>