<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function dd($data){
  highlight_string("<?php\n " . var_export($data, true) . "?>");
  echo '<script>document.getElementsByTagName("code")[0].getElementsByTagName("span")[1].remove() ;document.getElementsByTagName("code")[0].getElementsByTagName("span")[document.getElementsByTagName("code")[0].getElementsByTagName("span").length - 1].remove() ; </script>';
  die();
}

require_once 'vendor/autoload.php';

/*-------mysql--------------*/
$servername = "localhost";
$username = "imagesudip";
$password = "image@2020";
$dbname = "newimagedata";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

    $doctor_name = $_POST['doctor_name'];
    $email = $_POST['email'];
    $contact_no = $_POST['contact_no'];
    $address = $_POST['address'];
    $alt_address = $_POST['alt_address'];
    $patient_name = $_POST['patient_name'];
    $surgery_date = $_POST['surgery_date'];
    $total = $_POST['total'];
    $services = $_POST['services'];
    
    $message = $_POST['message'];
    
$sql = "INSERT INTO orders (doctor_name, email, contact_no, address, alt_address, patient_name, surgery_date, services, total)
VALUES ('$doctor_name', '$email', '$contact_no', '$address', '$alt_address', '$patient_name', '$surgery_date', '$services', '$total')";

if ($conn->query($sql) === TRUE) {
    $order_id = $conn->insert_id;
    /*echo "New record created successfully. Last inserted ID is: " . $last_id;*/
} else {
    /*echo "Error: " . $sql . "<br>" . $conn->error;*/
}

if(isset($order_id)) {
    $services = json_decode($services, true);
    
    if(isset($services['bone_segmentation']) && $services['bone_segmentation'] == 1) {
        $bone_segmentation = $_POST['bone_segmentation'];
        
        $basic_conversion = isset($bone_segmentation['basic_conversion']) ? $bone_segmentation['basic_conversion'] : 0;
        $virtual_extraction = isset($bone_segmentation['virtual_extraction']) ? $bone_segmentation['virtual_extraction'] : 0;
        $zygomatic_conversion = isset($bone_segmentation['zygomatic_conversion']) ? $bone_segmentation['zygomatic_conversion'] : 0;
        $jaw_information = isset($bone_segmentation['jaw_information']) ? $bone_segmentation['jaw_information'] : null;
        $order_description = isset($bone_segmentation['order_description']) ? $bone_segmentation['order_description'] : null;
        
        $sql = "INSERT INTO bone_segmentation (order_id, basic_conversion, virtual_extraction, zygomatic_conversion, jaw_information, order_description)
VALUES ('$order_id', '$basic_conversion', '$virtual_extraction', '$zygomatic_conversion', '$jaw_information', '$order_description')";
        if ($conn->query($sql) === TRUE) {
            /*echo "New record created successfully";*/
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
    
    if(isset($services['treatment_planning_design']) && $services['treatment_planning_design'] == 1) {
        $treatment_planning_design = $_POST['treatment_planning_design'];
        
        $bone_reduction_case = isset($treatment_planning_design['bone_reduction_case']) ? $treatment_planning_design['bone_reduction_case'] : 0;
        $tooth_supported_case = isset($treatment_planning_design['tooth_supported_case']) ? $treatment_planning_design['tooth_supported_case'] : 0;
        $tissue_supported_case = isset($treatment_planning_design['tissue_supported_case']) ? $treatment_planning_design['tissue_supported_case'] : 0;
        $bone_supported_case = isset($treatment_planning_design['bone_supported_case']) ? $treatment_planning_design['bone_supported_case'] : 0;
        $provisional_planning = isset($treatment_planning_design['provisional_planning']) ? $treatment_planning_design['provisional_planning'] : 0;
        $no_of_implants = isset($treatment_planning_design['no_of_implants']) ? (int)$treatment_planning_design['no_of_implants'] : 0;
        $preferred_implant = isset($treatment_planning_design['preferred_implant']) ? $treatment_planning_design['preferred_implant'] : null;
        $jaw_information = isset($treatment_planning_design['jaw_information']) ? $treatment_planning_design['jaw_information'] : null;
        $restorative_plan = isset($treatment_planning_design['restorative_plan']) ? $treatment_planning_design['restorative_plan'] : null;
        $surgery_date = isset($treatment_planning_design['surgery_date']) ? $treatment_planning_design['surgery_date'] : null;
        $provisional_planning_reqd = isset($treatment_planning_design['provisional_planning_reqd']) ? $treatment_planning_design['provisional_planning_reqd'] : null;
        
        $sql = "INSERT INTO treatment_planning_design (order_id, bone_reduction_case, tooth_supported_case, tissue_supported_case, bone_supported_case, provisional_planning, no_of_implants, preferred_implant, jaw_information, restorative_plan, surgery_date, provisional_planning_reqd)
VALUES ('$order_id', '$bone_reduction_case', '$tooth_supported_case', '$tissue_supported_case', '$bone_supported_case', '$provisional_planning', '$no_of_implants', '$preferred_implant', '$jaw_information', '$restorative_plan', '$surgery_date', '$provisional_planning_reqd')";
        if ($conn->query($sql) === TRUE) {
            /*echo "New record created successfully";*/
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
    
    if(isset($services['treatment_planning_design_fabrication']) && $services['treatment_planning_design_fabrication'] == 1) {
        $treatment_planning_design_fabrication = $_POST['treatment_planning_design_fabrication'];
        
        $bone_reduction_case = isset($treatment_planning_design_fabrication['bone_reduction_case']) ? $treatment_planning_design_fabrication['bone_reduction_case'] : 0;
        $tooth_supported_case = isset($treatment_planning_design_fabrication['tooth_supported_case']) ? $treatment_planning_design_fabrication['tooth_supported_case'] : 0;
        $tissue_supported_case = isset($treatment_planning_design_fabrication['tissue_supported_case']) ? $treatment_planning_design_fabrication['tissue_supported_case'] : 0;
        $bone_supported_case = isset($treatment_planning_design_fabrication['bone_supported_case']) ? $treatment_planning_design_fabrication['bone_supported_case'] : 0;
        $provisional_planning = isset($treatment_planning_design_fabrication['provisional_planning']) ? $treatment_planning_design_fabrication['provisional_planning'] : 0;
        $no_of_implants = isset($treatment_planning_design_fabrication['no_of_implants']) ? (int)$treatment_planning_design_fabrication['no_of_implants'] : 0;
        $preferred_implant = isset($treatment_planning_design_fabrication['preferred_implant']) ? $treatment_planning_design_fabrication['preferred_implant'] : null;
        $jaw_information = isset($treatment_planning_design_fabrication['jaw_information']) ? $treatment_planning_design_fabrication['jaw_information'] : null;
        $restorative_plan = isset($treatment_planning_design_fabrication['restorative_plan']) ? $treatment_planning_design_fabrication['restorative_plan'] : null;
        $surgery_date = isset($treatment_planning_design_fabrication['surgery_date']) ? $treatment_planning_design_fabrication['surgery_date'] : null;
        $provisional_planning_reqd = isset($treatment_planning_design_fabrication['provisional_planning_reqd']) ? $treatment_planning_design_fabrication['provisional_planning_reqd'] : null;
        
        $sql = "INSERT INTO treatment_planning_design_fabrication (order_id, bone_reduction_case, tooth_supported_case, tissue_supported_case, bone_supported_case, provisional_planning, no_of_implants, preferred_implant, jaw_information, restorative_plan, surgery_date, provisional_planning_reqd)
VALUES ('$order_id', '$bone_reduction_case', '$tooth_supported_case', '$tissue_supported_case', '$bone_supported_case', '$provisional_planning', '$no_of_implants', '$preferred_implant', '$jaw_information', '$restorative_plan', '$surgery_date', '$provisional_planning_reqd')";
        if ($conn->query($sql) === TRUE) {
            /*echo "New record created successfully";*/
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
    
    if(isset($services['digital_denture']) && $services['digital_denture'] == 1) {
        $digital_denture = $_POST['digital_denture'];
        
        $print = isset($digital_denture['print']) ? $digital_denture['print'] : 0;
        $printing_planning = isset($digital_denture['printing_planning']) ? $digital_denture['printing_planning'] : 0;
        $teeth_purchase_print = isset($digital_denture['teeth_purchase_print']) ? $digital_denture['teeth_purchase_print'] : null;
        
        $sql = "INSERT INTO digital_denture (order_id, print, printing_planning, teeth_purchase_print)
VALUES ('$order_id', '$print', '$printing_planning', '$teeth_purchase_print')";
        if ($conn->query($sql) === TRUE) {
            /*echo "New record created successfully";*/
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
    
    if(isset($services['orthodentic_treatment']) && $services['orthodentic_treatment'] == 1) {
        $orthodentic_treatment = $_POST['orthodentic_treatment'];
        
        $ortho_planning = isset($orthodentic_treatment['ortho_planning']) ? $orthodentic_treatment['ortho_planning'] : 0;
        $ortho_model_print = isset($orthodentic_treatment['ortho_model_print']) ? $orthodentic_treatment['ortho_model_print'] : 0;
        $no_of_prints = isset($orthodentic_treatment['no_of_prints']) ? (int)$orthodentic_treatment['no_of_prints'] : 0;
        $order_description = isset($orthodentic_treatment['order_description']) ? $orthodentic_treatment['order_description'] : null;
        $jaw_information = isset($orthodentic_treatment['jaw_information']) ? $orthodentic_treatment['jaw_information'] : null;
        $aligner_frequency = isset($orthodentic_treatment['aligner_frequency']) ? $orthodentic_treatment['aligner_frequency'] : null;
        $anterior_movement = isset($orthodentic_treatment['anterior_movement']) ? $orthodentic_treatment['anterior_movement'] : null;
        $chief_complaint = isset($orthodentic_treatment['chief_complaint']) ? $orthodentic_treatment['chief_complaint'] : null;
        
        $sql = "INSERT INTO orthodentic_treatment (order_id, ortho_planning, ortho_model_print, no_of_prints, order_description, jaw_information, aligner_frequency, anterior_movement, chief_complaint)
VALUES ('$order_id', '$ortho_planning', '$ortho_model_print', '$no_of_prints', '$order_description', '$jaw_information', '$aligner_frequency', '$anterior_movement', '$chief_complaint')";
        if ($conn->query($sql) === TRUE) {
            /*echo "New record created successfully";*/
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
    
    if(isset($services['radiology_report']) && $services['radiology_report'] == 1) {
        $radiology_report = $_POST['radiology_report'];
        
        $flat_rate = isset($radiology_report['flat_rate']) ? $radiology_report['flat_rate'] : 0;
        $study_of_purpose = isset($radiology_report['study_of_purpose']) ? $radiology_report['study_of_purpose'] : null;
        $job_description = isset($radiology_report['job_description']) ? $radiology_report['job_description'] : null;
        $date_of_birth = isset($radiology_report['date_of_birth']) ? $radiology_report['date_of_birth'] : null;
        
        $sql = "INSERT INTO radiology_report (order_id, flat_rate, study_of_purpose, job_description, date_of_birth)
VALUES ('$order_id', '$flat_rate', '$study_of_purpose', '$job_description', '$date_of_birth')";
        if ($conn->query($sql) === TRUE) {
            /*echo "New record created successfully";*/
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
    
    if(isset($services['metal_components']) && $services['metal_components'] == 1) {
        $metal_components = $_POST['metal_components'];
        
        $guide_tubes = isset($metal_components['guide_tubes']) ? $metal_components['guide_tubes'] : 0;
        $fixation_pins = isset($metal_components['fixation_pins']) ? $metal_components['fixation_pins'] : 0;
        
        $sql = "INSERT INTO metal_components (order_id, guide_tubes, fixation_pins)
VALUES ('$order_id', '$guide_tubes', '$fixation_pins')";
        if ($conn->query($sql) === TRUE) {
            /*echo "New record created successfully";*/
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
    
    if(isset($services['zygoma_guided_surgery']) && $services['zygoma_guided_surgery'] == 1) {
        $zygoma_guided_surgery = $_POST['zygoma_guided_surgery'];
        
        $quadrant_pterygoid = isset($zygoma_guided_surgery['quadrant_pterygoid']) ? $zygoma_guided_surgery['quadrant_pterygoid'] : 0;
        $full_pterygoid = isset($zygoma_guided_surgery['full_pterygoid']) ? $zygoma_guided_surgery['full_pterygoid'] : 0;
        
        $sql = "INSERT INTO zygoma_guided_surgery (order_id, quadrant_pterygoid, full_pterygoid)
VALUES ('$order_id', '$quadrant_pterygoid', '$full_pterygoid')";
        if ($conn->query($sql) === TRUE) {
            /*echo "New record created successfully";*/
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }


    $conn->close();
    
    /*----------Email-----------------*/
    // Create the Transport
    $transport = (new Swift_SmtpTransport('mail.image3dconversion.com', 587))
      ->setUsername('pankaj@image3dconversion.com')
      ->setPassword('pankaj');
    
    // Create the Mailer using your created Transport
    $mailer = new Swift_Mailer($transport);
    
    // To use the ArrayLogger
    /*$logger = new Swift_Plugins_Loggers_ArrayLogger();
    $mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));*/
    
    // Create a message
    $message = (new Swift_Message())
      // Give the message a subject
      ->setSubject('New Case - Order id #'.$order_id)
      // Set the From address with an associative array
      ->setFrom([$email => $doctor_name])
      // Set the To addresses with an associative array (setTo/setCc/setBcc)
      ->setTo(['info@image3dconversion.com', 'order@image3dconversion.com' , 'pankajcse409@gmail.com'])
      // Give it a body
      ->setBody($message, 'text/html')
      // And optionally an alternative body
      /*->addPart('<q>Here is the message itself</q>', 'text/html')*/
      // Optionally add any attachments
      /*->attach(Swift_Attachment::fromPath('my-document.pdf'))*/
      ;
    
    // Send the message
    $result = $mailer->send($message);
}
/*echo $logger->dump();*/
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="content-language" content="en-US">
        <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="/css/custom.css" type="text/css" />
        <style type="text/css">
            .card {
                margin-bottom:1em;
            }
        </style>
    </head>
    <body>
        <div class="container" style="margin-top:8%">
            <h4>Thank You for Placing Your Order</h4>
            <h1><b>Order Id: <?php echo($order_id) ?></b></h1>
            <hr/>
            <div class="bg-info col-md-12">
            <h5 style="text-align: left;" ><i>Note:</i> Kindly mail the requirements listed below to <strong>order@image3dconversion.com</strong> via wetransfer. Also mention your <b>Order Id</b> in the message</h5>
            </div>
            <br>
            <p style="text-align: left;margin-top:3%" >
                <strong>1. Bone Segmentation/Radiology report</strong><br>
            – CBCT Scan (in DICOM Format)<br><br>
                <strong>2. Guided Surgery (Dentulous Case)</strong><br>
            – Patient scan and digital scan of cast; OR<br>
            – Patient scan and impression scan<br><br>
                <strong>3. Guided Surgery (Dentulous Case – If Metal works are done previously)</strong><br>
            – Patient scan with impressions with markers; AND<br>
            – Impressions scan with markers only<br><br>
                <strong>4. Guided Surgery (Edentulous Case)</strong><br>
            – Patient scan with dentures/impressions with markers; AND<br>
            – Dentures/Impressions scan with markers only</p>
            
            
            <br>
           <a href="https://wetransfer.com/">
                <img src="wetransfer.png" width="200px">
            </a>
            
            
            
        </div>
    </body>
</html>